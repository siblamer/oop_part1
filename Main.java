public class Main {
    public static void main(String[] args) {
//        Task 1
        Test1 example1 = new Test1();
        example1.setA(1);
        example1.setB(2);

        System.out.println("Task1: \n"+example1);
        System.out.println("Maximum="+example1.getMaximum());

//        Task 2
        Test2 example2 = new Test2(10.,20.);
        example2.setA(1);
        example2.setB(2);
        System.out.println("\nTask2: \n" + example2);
        System.out.println("Maximum="+example2.getMaximum());

//        Task 3
        Triangle triangle = new Triangle(3,4,5);
        System.out.println(triangle);
        System.out.println("Area=" +triangle.getArea());
        System.out.println("Perimeter=" +triangle.getPerimeter());

//        Task 4
        DecimalCounter counter = new DecimalCounter(12);
        counter.increment();
        counter.increment();
        System.out.println("\nTask4: \n" + counter);
        counter.decrement();

//        Task 5

    }


    @Override
    public String toString() {
        return "Main{}";
    }

    @Override
    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}
