public class DecimalCounter {

    private int counter;
    private int default_value;
    public DecimalCounter(){
        this.counter = this.default_value;
    }

    public DecimalCounter(int value){
        this.counter = value;
    }

    public void increment(){
        this.counter++;
    }
    public void decrement(){
        this.counter--;
    }
    public void reset() {
        this.counter = this.default_value;
    }

    public int getCounter() {
        return counter;
    }

    @Override
    public String toString() {
        return "DecimalCounter{" +
                "counter=" + counter +
                '}';
    }
}
