public class OOP {

    private String hello_message = "Hello world";
    public static void main(String[] args) {

    }
    public void set_hellomessage(String hello_message){
        this.hello_message = hello_message;
    }

    public String get_hellomessage(){
        return this.hello_message;
    }

    @Override
    public String toString() {
        return "OOP{" +
                "hello_message='" + hello_message + '\'' +
                '}';
    }
}
